Todos.Todo = DS.Model.extend({
  title: DS.attr('string'),
  isCompleted: DS.attr('boolean')
});

Todos.Todo.FIXTURES = [
 {
   id: 1,
   title: 'Learn Ember.js',
   isCompleted: true
 },
 {
   id: 2,
   title: 'Determine if Ember suits this application',
   isCompleted: false
 },
 {
   id: 3,
   title: 'Get a better job',
   isCompleted: false
 }
];

